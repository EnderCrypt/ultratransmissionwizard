package endercrypt.utw.stages.primary;

import javafx.event.ActionEvent;

public interface PrimaryStageListener
{
	public void onLoadMessagesButton(ActionEvent event) throws Exception;

	public void onViewFilesButton(ActionEvent event) throws Exception;

	public void onUploadFileButton(ActionEvent event) throws Exception;

	public void onUploadDirectoryButton(ActionEvent event) throws Exception;
}
