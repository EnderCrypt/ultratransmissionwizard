package endercrypt.utw.stages.primary;

import endercrypt.utw.api.DiscordMessageObject;
import endercrypt.utw.api.model.Transmission;
import endercrypt.utw.stages.StageWindow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.dv8tion.jda.api.entities.User;

public class PrimaryStage extends StageWindow
{
	public final ListView<DiscordMessageObject<Transmission>> transmissionListView;
	public final Button loadMessagesButton;
	public final Button viewFilesButton;
	public final Button uploadFileButton;
	public final Button uploadDirectoryButton;

	public PrimaryStage(PrimaryStageListener listener, Stage stage, String title)
	{
		super(stage, title);

		// items list
		transmissionListView = new ListView<>();
		transmissionListView.setCellFactory(new Callback<ListView<DiscordMessageObject<Transmission>>, ListCell<DiscordMessageObject<Transmission>>>()
		{
			@Override
			public ListCell<DiscordMessageObject<Transmission>> call(ListView<DiscordMessageObject<Transmission>> parameter)
			{
				return new ListCell<DiscordMessageObject<Transmission>>()
				{
					@Override
					protected void updateItem(DiscordMessageObject<Transmission> item, boolean empty)
					{
						super.updateItem(item, empty);
						if (item != null)
						{
							Transmission transmissionMetadata = item.getObject();
							User author = item.getMessage().getAuthor();
							setText(transmissionMetadata.getName() + " by " + author.getAsTag());
						}
						else
						{
							setText("");
						}
					}
				};
			}
		});

		transmissionListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<DiscordMessageObject<Transmission>>()
		{
			@Override
			public void changed(ObservableValue<? extends DiscordMessageObject<Transmission>> observable, DiscordMessageObject<Transmission> oldValue, DiscordMessageObject<Transmission> newValue)
			{
				viewFilesButton.setDisable(newValue == null);
			}
		});

		// load message button
		loadMessagesButton = new Button("Load message");
		loadMessagesButton.setOnAction(event -> callStageListener(listener::onLoadMessagesButton, event));

		VBox listViewBox = new VBox(10);
		listViewBox.getChildren().addAll(loadMessagesButton, transmissionListView);

		// side box
		VBox sideBox = new VBox(10);

		// download
		viewFilesButton = new Button("View files");
		viewFilesButton.setMaxWidth(Double.MAX_VALUE);
		viewFilesButton.setDisable(true);
		viewFilesButton.setOnAction(event -> callStageListener(listener::onViewFilesButton, event));

		// upload
		uploadFileButton = new Button("File");
		uploadFileButton.setMaxWidth(Double.MAX_VALUE);
		uploadFileButton.setOnAction(event -> callStageListener(listener::onUploadFileButton, event));

		uploadDirectoryButton = new Button("Directory");
		uploadDirectoryButton.setMaxWidth(Double.MAX_VALUE);
		uploadDirectoryButton.setOnAction(event -> callStageListener(listener::onUploadDirectoryButton, event));

		// upload box
		VBox uploadBox = new VBox(10);
		uploadBox.getChildren().addAll(uploadFileButton, uploadDirectoryButton);

		TitledPane uploadPane = new TitledPane();
		uploadPane.setText("Upload");
		uploadPane.setContent(uploadBox);

		// assemble stage
		HBox parentBox = new HBox(10);
		parentBox.setPadding(new Insets(10));
		sideBox.getChildren().addAll(viewFilesButton, uploadPane);
		parentBox.getChildren().addAll(listViewBox, sideBox);
		Scene scene = new Scene(parentBox);
		stage.setScene(scene);
		stage.setTitle(title);
		stage.centerOnScreen();
		stage.show();
	}
}
