package endercrypt.utw.stages;

import endercrypt.utw.utility.Protector;
import endercrypt.utw.utility.interfaces.ExceptionConsumer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

public class StageWindow
{
	protected final Stage stage;

	public StageWindow()
	{
		this("");
	}

	public StageWindow(String title)
	{
		this(new Stage(), title);
	}

	public StageWindow(Stage stage, String title)
	{
		this.stage = stage;
		this.stage.setTitle(title);
	}

	protected static void callStageListener(ExceptionConsumer<ActionEvent> method, ActionEvent event)
	{
		Protector.executorRun(false, () -> method.accept(event));
	}

	public void openStage(ExceptionConsumer<Stage> consumer)
	{
		Platform.runLater(() -> {
			Protector.run(false, () -> {
				consumer.accept(stage);
			});
		});
	}
}
