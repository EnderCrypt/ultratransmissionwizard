package endercrypt.utw.stages.viewfile;

import java.io.File;
import java.nio.file.Path;

import org.apache.commons.io.FilenameUtils;

import endercrypt.utw.api.DiscordMessageObject;
import endercrypt.utw.api.model.Transmission;
import endercrypt.utw.api.model.TransmissionPath;
import endercrypt.utw.stages.StageWindow;
import endercrypt.utw.utility.Protector;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public class ViewFileStage extends StageWindow
{
	public final GridPane infoBox;

	public final Text filenameText;
	public final Text fileExtensionText;
	public final Text fileSizeText;

	public ViewFileStage(ViewFileStageListener listener, DiscordMessageObject<Transmission> discordTransmission)
	{
		super();

		Transmission transmission = discordTransmission.getObject();
		Message message = discordTransmission.getMessage();
		User user = message.getAuthor();

		// LEFT //
		VBox leftBox = new VBox(10);

		TreeView<TransmissionPath> treeView = new TreeView<>(transmission.getPath().toTreeItem());
		treeView.setMinWidth(500);
		treeView.setMinHeight(500);
		leftBox.getChildren().addAll(treeView);

		treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<TransmissionPath>>()
		{
			@Override
			public void changed(ObservableValue<? extends TreeItem<TransmissionPath>> observable, TreeItem<TransmissionPath> oldValue, TreeItem<TransmissionPath> newValue)
			{
				TransmissionPath transmissionPath = newValue.getValue();
				if (transmissionPath != null)
				{
					filenameText.setText(FilenameUtils.getBaseName(transmissionPath.getName()));
					fileExtensionText.setText(FilenameUtils.getExtension(transmissionPath.getName()));
					fileSizeText.setText(transmissionPath.toFilesizeString() + " (" + transmissionPath.getSize() + " bytes)");
				}
			}
		});

		// RIGHT //
		VBox rightBox = new VBox(10);

		Button downloadButton = new Button("Download");
		downloadButton.setMaxWidth(Double.MAX_VALUE);
		downloadButton.setOnAction(event -> {
			Platform.runLater(() -> {
				Protector.run(false, () -> {
					DirectoryChooser directoryChooser = new DirectoryChooser();
					directoryChooser.setTitle("Choose download destination");
					File file = directoryChooser.showDialog(stage);
					if (file == null)
					{
						return;
					}
					Path path = file.toPath();

					stage.close();
					Protector.executorRun(false, () -> listener.onDownloadTransmission(transmission, path));
				});
			});
		});

		infoBox = new GridPane();
		int i = 0;
		filenameText = addInfoField("Filename", i++);
		fileExtensionText = addInfoField("Extension", i++);
		fileSizeText = addInfoField("Size", i++);

		rightBox.getChildren().addAll(downloadButton, new TitledPane("Info", infoBox));

		// FINALIZE //

		treeView.getSelectionModel().selectFirst();

		stage.setTitle("Viewing " + transmission.getName() + " by " + user.getAsTag());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UNIFIED);
		HBox root = new HBox(10);
		root.setPadding(new Insets(10));
		root.getChildren().addAll(leftBox, rightBox);
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.centerOnScreen();
	}

	private Text addInfoField(String text, int index)
	{
		Text entryText = new Text(text + ": ");
		GridPane.setColumnIndex(entryText, 0);
		GridPane.setRowIndex(entryText, index);

		Text valueText = new Text("");
		GridPane.setColumnIndex(valueText, 1);
		GridPane.setRowIndex(valueText, index);

		infoBox.getChildren().addAll(entryText, valueText);

		return valueText;
	}
}
