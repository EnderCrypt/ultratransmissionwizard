package endercrypt.utw.stages.viewfile;

import java.nio.file.Path;

import endercrypt.utw.api.model.Transmission;

public interface ViewFileStageListener
{
	public void onDownloadTransmission(Transmission transmission, Path destination) throws Exception;
}
