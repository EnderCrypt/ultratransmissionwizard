package endercrypt.utw.discord.messagesearcher;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

public class DiscordDirectMessageSearcher extends DiscordMessageSearcher
{
	private MessageChannel channel;

	public DiscordDirectMessageSearcher(MessageChannel channel)
	{
		this.channel = channel;
	}

	@Override
	public Message internalFind(long id)
	{
		return channel.getHistoryAround(id, 1).complete().getMessageById(id);
	}
}
