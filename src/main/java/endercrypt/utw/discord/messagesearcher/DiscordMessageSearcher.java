package endercrypt.utw.discord.messagesearcher;

import java.util.NoSuchElementException;

import net.dv8tion.jda.api.entities.Message;

public abstract class DiscordMessageSearcher
{
	public Message find(long id)
	{
		if (id <= 0)
		{
			throw new IllegalArgumentException("id cant be 0 or negative");
		}
		Message message = internalFind(id);
		if (message == null)
		{
			notFound(id);
		}
		return message;
	}

	protected void notFound(long id)
	{
		throw new NoSuchElementException("Message with id " + id + " not found when trying to assemble chunks");
	}

	protected abstract Message internalFind(long id);
}
