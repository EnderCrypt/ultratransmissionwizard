package endercrypt.utw.discord.messagesearcher;

import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageHistory;

/*
 * efficient for big transfers but risks not finding the message
 */
@Deprecated
public class DiscordBulkMessageSearcher extends DiscordMessageSearcher
{
	private static final int MAX = 200;
	private static final int PAGE = 20;

	private Message indexMessage;
	private MessageChannel channel;
	private List<Message> history = new ArrayList<>();

	public DiscordBulkMessageSearcher(Message message)
	{
		this.indexMessage = message;
		this.channel = message.getChannel();
	}

	private boolean pageHistory()
	{
		if (history.size() + PAGE > MAX)
		{
			return false;
		}
		MessageHistory messageHistory = channel.getHistoryBefore(indexMessage, PAGE).complete();
		history.addAll(messageHistory.getRetrievedHistory());
		indexMessage = history.get(history.size() - 1);
		return true;
	}

	@Override
	protected Message internalFind(long id)
	{
		while (true)
		{
			// find message
			for (Message message : history)
			{
				if (message.getIdLong() == id)
				{
					return message;
				}
			}
			// page
			if (pageHistory() == false)
			{
				notFound(id);
			}
		}
	}
}
