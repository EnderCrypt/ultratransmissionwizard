package endercrypt.utw.discord;

import javax.security.auth.login.LoginException;

import endercrypt.utw.utility.JavaFxUtility;
import endercrypt.utw.utility.JavaFxUtility.TaskPopup;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.SelfUser;

public class Discord
{
	private JDA jda;

	public Discord(AccountType accountType, String token) throws LoginException, InterruptedException
	{
		if (token.length() != 59)
		{
			throw new LoginException("Token must be 59 characters long");
		}
		TaskPopup taskPopup = JavaFxUtility.taskPopup("Discord", "Signing in...", true);
		try
		{
			JDABuilder jdaBuilder = new JDABuilder(accountType);
			jdaBuilder.setToken(token);
			jda = jdaBuilder.build();
			taskPopup.setText("Login successful, loading...");
			jda.awaitReady();
		}
		finally
		{
			taskPopup.close();
		}
	}

	public JDA getJda()
	{
		return jda;
	}

	public SelfUser getSelfUser()
	{
		return getJda().getSelfUser();
	}

	public void addEventListener(Object object)
	{
		getJda().addEventListener(object);
	}
}
