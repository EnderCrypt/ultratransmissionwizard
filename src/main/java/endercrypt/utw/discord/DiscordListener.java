package endercrypt.utw.discord;

import endercrypt.utw.api.UltraTransmissionWizard;
import endercrypt.utw.utility.Protector;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.SelfUser;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class DiscordListener extends ListenerAdapter
{
	@Override
	public void onPrivateMessageReceived(PrivateMessageReceivedEvent event)
	{
		Protector.run(false, () -> {
			JDA jda = event.getJDA();
			SelfUser me = jda.getSelfUser();
			User author = event.getAuthor();
			Message message = event.getMessage();
			if (author.equals(me))
			{
				return;
			}
			UltraTransmissionWizard.register(message);
		});
	}
}
