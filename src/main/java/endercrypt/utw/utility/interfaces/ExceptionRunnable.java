package endercrypt.utw.utility.interfaces;

public interface ExceptionRunnable
{
	void run() throws Exception;
}
