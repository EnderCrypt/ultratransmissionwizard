package endercrypt.utw.utility.interfaces;

public interface ExceptionConsumer<T>
{
	void accept(T t) throws Exception;
}
