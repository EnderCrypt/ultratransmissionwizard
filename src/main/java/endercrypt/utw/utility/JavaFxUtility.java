package endercrypt.utw.utility;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.exception.ExceptionUtils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class JavaFxUtility
{
	private static Stage emptyPopup(String title, Scene scene)
	{
		Stage stage = new Stage();

		stage.setResizable(false);
		stage.setTitle(title);
		stage.setMinWidth(500);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UNIFIED);
		stage.setScene(scene);
		stage.centerOnScreen();

		return stage;
	}

	public static <T> T platform(Callable<T> callable)
	{
		Object lock = new Object();
		AtomicBoolean done = new AtomicBoolean(false);
		AtomicReference<T> reference = new AtomicReference<>(null);

		Platform.runLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					reference.set(callable.call());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					done.set(true);
					synchronized (lock)
					{
						lock.notifyAll();
					}
				}
			}
		});
		try
		{
			synchronized (lock)
			{
				while (done.get() == false)
				{
					lock.wait();
				}
			}
		}
		catch (InterruptedException e)
		{
			// ignore
		}

		return reference.get();
	}

	public static boolean passwordPrompt(String title, String message, PasswordStorage passwordStorage) throws InterruptedException
	{
		AtomicBoolean accepted = new AtomicBoolean(true);
		BasicLock lock = new BasicLock();

		Platform.runLater(() -> {
			// parent
			VBox vbox = new VBox(10);
			vbox.setPadding(new Insets(10));

			// stage & scene
			Scene scene = new Scene(vbox);
			Stage stage = emptyPopup(title, scene);

			// on close
			stage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent event)
				{
					accepted.set(false);
				}
			});

			// form
			Label messageLabel = new Label(message);
			messageLabel.setTextAlignment(TextAlignment.CENTER);

			CheckBox rememberCheckbox = new CheckBox();
			rememberCheckbox.setSelected(passwordStorage.isRemember());
			rememberCheckbox.setText("Remember password (Plain text!)");
			rememberCheckbox.selectedProperty().addListener((observable) -> {
				passwordStorage.setRemember(rememberCheckbox.isSelected());
			});

			PasswordField passwordField = new PasswordField();
			if (passwordStorage.hasPassword())
			{
				passwordField.setText(passwordStorage.getPassword());
			}
			passwordField.setPromptText("Password");
			passwordField.textProperty().addListener((observable) -> {
				passwordStorage.setPassword(passwordField.getText());
			});

			Button loginButton = new Button("Login");
			loginButton.setMaxWidth(Double.MAX_VALUE);
			loginButton.setOnAction((event) -> {
				stage.close();
			});

			Button cancelButton = new Button("Cancel");
			cancelButton.setMaxWidth(Double.MAX_VALUE);
			cancelButton.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent event)
				{
					accepted.set(false);

					stage.close();
				}
			});

			// finalize
			vbox.getChildren().addAll(messageLabel, passwordField, rememberCheckbox, loginButton, cancelButton);

			// show
			stage.showAndWait();

			lock.unlock();
		});

		lock.waitForUnlocked();
		return accepted.get();
	}

	public static Optional<String> promptPopup(String title, String message, String defaultText, String placeholderText)
	{
		return platform(() -> {
			// parent
			VBox vbox = new VBox(10);
			vbox.setPadding(new Insets(10));

			// stage & scene
			Scene scene = new Scene(vbox);
			Stage stage = emptyPopup(title, scene);

			// on close
			AtomicBoolean cancel = new AtomicBoolean(false);
			stage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent event)
				{
					cancel.set(true);
				}
			});

			// form
			Label messageLabel = new Label(message);
			messageLabel.setTextAlignment(TextAlignment.CENTER);

			TextField inputField = new TextField();
			inputField.setText(defaultText);
			inputField.setPromptText(placeholderText);

			Button acceptButton = new Button("Accept");
			acceptButton.setMaxWidth(Double.MAX_VALUE);
			acceptButton.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent event)
				{
					stage.close();
				}
			});

			Button cancelButton = new Button("Cancel");
			cancelButton.setMaxWidth(Double.MAX_VALUE);
			cancelButton.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent event)
				{
					cancel.set(true);

					stage.close();
				}
			});

			// finalize
			vbox.getChildren().addAll(messageLabel, inputField, acceptButton, cancelButton);

			// show
			stage.showAndWait();

			// return
			if (cancel.get())
			{
				return Optional.empty();
			}
			else
			{
				return Optional.of(inputField.getText() == null ? "" : inputField.getText());
			}
		});
	}

	public static void messagePopup(String title, String message)
	{
		platform(() -> {
			// parent
			VBox vbox = new VBox(10);
			vbox.setPadding(new Insets(10));

			// stage & scene
			Scene scene = new Scene(vbox);
			final Stage stage = emptyPopup(title, scene);

			// form
			Label messageLabel = new Label(message);
			messageLabel.setTextAlignment(TextAlignment.CENTER);

			Button okButton = new Button("Ok");
			okButton.setMaxWidth(Double.MAX_VALUE);
			okButton.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent event)
				{
					stage.close();
				}
			});

			// finalize
			vbox.getChildren().addAll(messageLabel, okButton);

			// show
			stage.showAndWait();

			return null;
		});
	}

	public interface TaskPopup
	{
		public void setProgress(double value);

		public void setText(String value);

		public boolean isClosed();

		public void close();
	}

	public static TaskPopup taskPopup(String title, String info, boolean required)
	{
		return platform(() -> {
			// parent
			VBox vbox = new VBox(10);
			vbox.setPadding(new Insets(10));

			// stage & scene
			Scene scene = new Scene(vbox);
			final Stage stage = emptyPopup(title, scene);
			stage.setMinWidth(250);

			AtomicBoolean isClosed = new AtomicBoolean(false);
			stage.setOnCloseRequest(e -> {
				if (required)
				{
					e.consume();
				}
				else
				{
					isClosed.set(true);
				}
			});

			// form
			Text infoField = new Text(info);
			infoField.setTextAlignment(TextAlignment.CENTER);

			ProgressBar progressBar = new ProgressBar();
			progressBar.setPrefWidth(500);

			// finalize
			vbox.getChildren().addAll(infoField, progressBar);

			// show
			stage.show();

			// return
			return new TaskPopup()
			{
				@Override
				public void setProgress(double value)
				{
					Platform.runLater(() -> progressBar.setProgress(value));
				}

				@Override
				public void setText(String value)
				{
					Platform.runLater(() -> {
						infoField.setText(value);
					});
				}

				@Override
				public boolean isClosed()
				{
					return isClosed.get();
				}

				@Override
				public void close()
				{
					Platform.runLater(() -> stage.close());
				}
			};
		});
	}

	public static void errorPopup(Throwable e)
	{
		messagePopup(e.getClass().getName(), ExceptionUtils.getStackTrace(e));
	}
}
