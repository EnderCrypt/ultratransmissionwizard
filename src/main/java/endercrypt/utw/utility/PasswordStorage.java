package endercrypt.utw.utility;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PasswordStorage
{
	private final Path path;
	private String password = null;
	private boolean remember = false;

	public PasswordStorage(Path path) throws IOException
	{
		this.path = path;
		this.remember = Files.exists(this.path);
		if (Files.exists(this.path))
		{
			this.password = Files.readString(path);
		}
		trigger();
	}

	public void trigger()
	{
		Protector.run(false, () -> {
			if (remember && password != null)
			{
				Files.write(path, password.getBytes());
			}
			else
			{
				Files.deleteIfExists(path);
			}
		});
	}

	public void setPassword(String password)
	{
		if (password.equals(""))
		{
			password = null;
		}
		this.password = password;
		trigger();
	}

	public String getPassword()
	{
		return password;
	}

	public boolean hasPassword()
	{
		return getPassword() != null;
	}

	public void setRemember(boolean remember)
	{
		this.remember = remember;
		trigger();
	}

	public boolean isRemember()
	{
		return remember;
	}
}
