package endercrypt.utw.utility;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.gson.Gson;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

public class Utility
{
	public static final ExecutorService executor = Executors.newCachedThreadPool();

	public static final Gson gson = new Gson();

	// resource
	public static InputStream getResourceStream(String path)
	{
		return Utility.class.getResourceAsStream(path);
	}

	// math
	public static double round(double value, int decimals)
	{
		double multiplier = Math.pow(10, decimals);

		return Math.round(value * multiplier) / multiplier;
	}

	// file
	public static File generateTempFile()
	{
		return generateTempFile("temp");
	}

	public static File generateTempFile(String extension)
	{
		return new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString() + "." + extension);
	}

	// zip
	public static void compressFile(File input, File output) throws ZipException
	{
		ZipParameters parameters = new ZipParameters();
		parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
		parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

		ZipFile zipFile = new ZipFile(output);

		if (input.isFile())
		{
			zipFile.addFile(input, parameters);
		}
		if (input.isDirectory())
		{
			zipFile.addFolder(input, parameters);
		}
	}

	public static void decompressFile(File input, Path destination) throws ZipException
	{
		ZipFile zipFile = new ZipFile(input);
		zipFile.extractAll(destination.toString());
	}
}
