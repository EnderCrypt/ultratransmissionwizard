package endercrypt.utw.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nullable;

public class EventListener<T>
{
	private List<T> listeners = new ArrayList<>();

	public void add(T listener)
	{
		listeners.add(listener);
	}

	public boolean remove(T listener)
	{
		return listeners.remove(listener);
	}

	public void trigger(Consumer<T> consumer)
	{
		trigger(consumer, null);
	}

	public void trigger(Consumer<T> consumer, @Nullable Consumer<Exception> errorConsumer)
	{
		for (T listener : listeners)
		{
			try
			{
				consumer.accept(listener);
			}
			catch (Exception e)
			{
				if (errorConsumer == null)
				{
					e.printStackTrace();
				}
				else
				{
					errorConsumer.accept(e);
				}
			}
		}
	}
}
