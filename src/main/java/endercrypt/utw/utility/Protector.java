package endercrypt.utw.utility;

import java.util.Optional;
import java.util.concurrent.Callable;

import endercrypt.utw.utility.interfaces.ExceptionRunnable;

public class Protector
{
	public static void executorRun(boolean critical, ExceptionRunnable code)
	{
		Utility.executor.execute(() -> {
			run(critical, code);
		});
	}

	public static Optional<Exception> run(boolean critical, ExceptionRunnable code)
	{
		Result<Void> result = run(critical, new Callable<Void>()
		{
			@Override
			public Void call() throws Exception
			{
				code.run();
				return null;
			}
		});
		return Optional.ofNullable(result.getException());
	}

	public static <T> Result<T> run(boolean critical, Callable<T> code)
	{
		try
		{
			return new Result<>(code.call());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JavaFxUtility.errorPopup(e);
			if (critical)
			{
				System.exit(1);
				return null;
			}
			else
			{
				return new Result<>(e);
			}
		}
	}

	public static class Result<T>
	{
		private final Exception exception;

		private final T result;

		private Result(T result)
		{
			this.exception = null;
			this.result = result;
		}

		private Result(Exception exception)
		{
			this.exception = exception;
			this.result = null;
		}

		public boolean isSuccess()
		{
			return exception == null;
		}

		public boolean isFail()
		{
			return isSuccess() == false;
		}

		public Exception getException()
		{
			return exception;
		}

		public T getResult()
		{
			return result;
		}
	}
}
