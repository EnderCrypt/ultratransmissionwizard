package endercrypt.utw;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static java.util.concurrent.TimeUnit.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.security.auth.login.LoginException;

import endercrypt.utw.api.DiscordMessageObject;
import endercrypt.utw.api.UltraTransmissionWizard;
import endercrypt.utw.api.event.TransmissionReceivedEvent;
import endercrypt.utw.api.model.Transmission;
import endercrypt.utw.discord.Discord;
import endercrypt.utw.discord.DiscordListener;
import endercrypt.utw.stages.primary.PrimaryStage;
import endercrypt.utw.stages.primary.PrimaryStageListener;
import endercrypt.utw.stages.viewfile.ViewFileStage;
import endercrypt.utw.stages.viewfile.ViewFileStageListener;
import endercrypt.utw.utility.JavaFxUtility;
import endercrypt.utw.utility.JavaFxUtility.TaskPopup;
import endercrypt.utw.utility.PasswordStorage;
import endercrypt.utw.utility.Protector;
import endercrypt.utw.utility.Utility;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.scene.control.MultipleSelectionModel;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.lingala.zip4j.exception.ZipException;

public class Main extends Application implements PrimaryStageListener, ViewFileStageListener
{
	public static final String name = "Discord Ultra Transmission Wizard";

	public static void main(String[] args)
	{
		launch(args);
	}

	private PrimaryStage primary;
	private Discord discord;

	@Override
	public void start(Stage stage) throws Exception
	{
		this.primary = new PrimaryStage(this, stage, name);

		// discord
		Protector.executorRun(false, () -> {
			stageDiscordLogin();

			stageDiscordOnline();
		});
	}

	public void stageDiscordLogin() throws InterruptedException, IOException
	{
		while (true)
		{
			// discord token
			PasswordStorage passwordStorage = new PasswordStorage(Paths.get(".discord token.txt"));
			if (JavaFxUtility.passwordPrompt("Discord", "Please input your discord token", passwordStorage) == false)
			{
				JavaFxUtility.messagePopup("Discord", "well, goobye then");
				primary.openStage((stage) -> stage.close());
				return;
			}

			// discord login
			try
			{
				discord = new Discord(AccountType.CLIENT, passwordStorage.getPassword());
				// JavaFxUtility.messagePopup("Discord", "Login successful");
				break;
			}
			catch (LoginException | InterruptedException e)
			{
				e.printStackTrace();
				JavaFxUtility.messagePopup("Discord", "Login failed!");
			}
		}

		// api
		UltraTransmissionWizard.onTransmissionReceivedEvent.add(new TransmissionReceivedEvent()
		{
			@Override
			public void onEvent(DiscordMessageObject<Transmission> transmissionMetadata)
			{
				primary.transmissionListView.getItems().add(transmissionMetadata);
			}
		});

		// finalize discord
		discord.addEventListener(new DiscordListener());
	}

	public void stageDiscordOnline() throws InterruptedException, IOException, ExecutionException
	{
		TaskPopup taskPopup = JavaFxUtility.taskPopup("Discord", "Reading history", false);
		List<PrivateChannel> channels = discord.getJda().getPrivateChannels();
		int i = 0;
		for (PrivateChannel channel : channels)
		{
			taskPopup.setText("Reading history of " + channel.getUser().getAsTag());
			taskPopup.setProgress(1.0 / channels.size() * i);
			i++;
			if (channel.hasLatestMessage())
			{
				long lastMessage = channel.getLatestMessageIdLong();
				List<Message> history = channel.getHistoryBefore(lastMessage, 20).complete().getRetrievedHistory();

				for (Message message : history)
				{
					UltraTransmissionWizard.register(message);
				}
				System.out.println("Scanned " + history.size() + " messages from " + channel.getUser().getAsTag());

				Thread.sleep(100);
			}
		}
		System.out.println("Done scanning history");
		taskPopup.close();
	}

	@Override
	public void stop() throws Exception
	{
		final int seconds = 10;
		System.out.println("shutting down...");
		if (Utility.executor.awaitTermination(seconds, SECONDS))
		{
			System.out.println("Shutdown complete!");
		}
		else
		{
			System.out.println("Shutdown timeout, force quitting!");
		}
		System.exit(0);
	}

	@Override
	public void onLoadMessagesButton(ActionEvent event) throws IOException, InterruptedException, ExecutionException
	{
		long id = -1L;
		while (true)
		{
			try
			{
				Optional<String> idOptional = JavaFxUtility.promptPopup("Discord", "Please enter a message ID to scan", null, "message id");
				if (idOptional.isPresent())
				{
					id = Long.parseLong(idOptional.get());
					break;
				}
				else
				{
					return;
				}
			}
			catch (NumberFormatException e)
			{
				JavaFxUtility.messagePopup("Discord", "Not a valid message ID");
			}
		}
		TaskPopup taskPopup = JavaFxUtility.taskPopup("Searching for " + id, "", false);
		List<PrivateChannel> channels = discord.getJda().getPrivateChannels();
		Message message = null;
		int i = 0;
		for (PrivateChannel channel : channels)
		{
			taskPopup.setText(channel.getUser().getAsTag());
			taskPopup.setProgress(1.0 / channels.size() * i);
			i++;
			message = channel.getHistoryAround(id, 1).complete().getMessageById(id);
			if (message != null)
			{
				break;
			}
		}
		taskPopup.close();
		if (message == null)
		{
			JavaFxUtility.messagePopup("Discord", "Sorry, but the message was not found!\nare you sure you used the id of the message with the filename " + Transmission.FILENAME);
		}
		else
		{
			if (UltraTransmissionWizard.register(message) == false)
			{
				JavaFxUtility.messagePopup("Discord", "Transmission was found but was not valid");
			}
		}
		System.out.println("Done scanning history");
	}

	@Override
	public void onViewFilesButton(ActionEvent event)
	{
		primary.openStage((stage) -> {
			ObjectProperty<MultipleSelectionModel<DiscordMessageObject<Transmission>>> listSelectionProperty = primary.transmissionListView.selectionModelProperty();
			DiscordMessageObject<Transmission> discordTransmission = listSelectionProperty.getValue().getSelectedItem();
			ViewFileStage viewFileWindow = new ViewFileStage(this, discordTransmission);
			viewFileWindow.openStage((viewFileStage) -> {
				viewFileStage.showAndWait();
			});

		});
	}

	@Override
	public void onUploadFileButton(ActionEvent event)
	{
		primary.openStage((stage) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Choose file");
			File file = fileChooser.showOpenDialog(stage);

			Protector.executorRun(false, () -> uploadStage(file));
		});
	}

	@Override
	public void onUploadDirectoryButton(ActionEvent event)
	{
		primary.openStage((stage) -> {
			DirectoryChooser directoryChooser = new DirectoryChooser();
			directoryChooser.setTitle("Choose Directory");
			File file = directoryChooser.showDialog(stage);

			Protector.executorRun(false, () -> uploadStage(file));
		});
	}

	private void uploadStage(File file) throws IOException, InterruptedException, ExecutionException, ZipException
	{
		// cancelled 
		if (file == null)
		{
			return;
		}

		// nitro
		UltraTransmissionWizard.setChunkSize(discord.getJda().getSelfUser().isNitro() ? UltraTransmissionWizard.FILESIZE_NITRO : UltraTransmissionWizard.FILESIZE_STANDARD);

		// transmission name
		Optional<String> nameOptional = JavaFxUtility.promptPopup("Transmission of " + file.getName(), "Name of transmission", file.getName(), null);
		if (nameOptional.isEmpty())
		{
			JavaFxUtility.messagePopup("Transmission of " + file.getName(), "Transmission cancelled");
			return;
		}
		String transmissionName = nameOptional.get();

		// user selection
		User user = discord.getJda().getPrivateChannels().stream().map(c -> c.getUser()).filter(u -> u.getIdLong() == 171199917392396289L).findAny().get();

		// task popup
		TaskPopup taskPopup = JavaFxUtility.taskPopup("Transmission of " + transmissionName, "Starting...", false);

		// send
		UltraTransmissionWizard.send(transmissionName, file, user, taskPopup);

		// finish
		taskPopup.close();
	}

	@Override
	public void onDownloadTransmission(Transmission transmission, Path destination) throws Exception
	{
		// task popup
		TaskPopup taskPopup = JavaFxUtility.taskPopup("Downloading " + transmission.getName() + " to " + destination, "Starting...", false);

		// send
		UltraTransmissionWizard.receive(transmission, destination, taskPopup);

		// finish
		taskPopup.close();
	}
}
