package endercrypt.utw.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonObject;
import endercrypt.utw.api.event.TransmissionReceivedEvent;
import endercrypt.utw.api.model.TransmissionChunk;
import endercrypt.utw.api.model.TransmissionPath;
import endercrypt.utw.api.model.Transmission;
import endercrypt.utw.utility.EventListener;
import endercrypt.utw.utility.JavaFxUtility.TaskPopup;
import endercrypt.utw.utility.Utility;
import javafx.application.Platform;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.lingala.zip4j.exception.ZipException;
import net.dv8tion.jda.api.entities.Message.Attachment;

public class UltraTransmissionWizard
{
	public static int MEGABYTE = (1024 * 1024);
	public static int FILESIZE_STANDARD = (MEGABYTE * 8) - 1024;
	public static int FILESIZE_NITRO = (MEGABYTE * 50) - 1024;

	private static int chunkSize = FILESIZE_STANDARD;

	public static final List<DiscordMessageObject<Transmission>> transmissions = new ArrayList<>();
	public static final EventListener<TransmissionReceivedEvent> onTransmissionReceivedEvent = new EventListener<>();

	public static synchronized boolean register(Message message) throws IOException, InterruptedException, ExecutionException
	{
		// attachment
		if (message.getAttachments().size() != 1)
		{
			return false;
		}
		Attachment attachment = message.getAttachments().get(0);
		if (attachment.getFileName().equals(Transmission.FILENAME) == false)
		{
			return false;
		}

		// if already registered
		if (transmissions.stream().filter(dmo -> dmo.getMessage().getIdLong() == message.getIdLong()).findAny().isPresent())
		{
			return false;
		}

		// json
		InputStream inputStream = attachment.retrieveInputStream().get();
		String rawJson = IOUtils.toString(inputStream, Charset.defaultCharset());
		JsonObject jsonRoot = Utility.gson.fromJson(rawJson, JsonObject.class);

		// transmission
		Transmission transmissionMetadata = null;
		try
		{
			transmissionMetadata = new Transmission(jsonRoot, message);
		}
		catch (NullPointerException | IllegalStateException e)
		{
			return false;
		}
		DiscordMessageObject<Transmission> discordTransmissionMetadata = new DiscordMessageObject<>(transmissionMetadata, message);
		Platform.runLater(() -> {
			onTransmissionReceivedEvent.trigger(new Consumer<TransmissionReceivedEvent>()
			{
				@Override
				public void accept(TransmissionReceivedEvent transmissionReceivedEvent)
				{
					transmissionReceivedEvent.onEvent(discordTransmissionMetadata);
				}
			});
		});
		return true;
	}

	public static void setChunkSize(int bytes)
	{
		UltraTransmissionWizard.chunkSize = bytes;
	}

	private static DiscordMessageObject<Transmission> sendMetadata(MessageChannel channel, Transmission transmission)
	{
		Message message = channel.sendFile(transmission.toEncodedJsonString().getBytes(), Transmission.FILENAME).complete();
		return new DiscordMessageObject<>(transmission, message);
	}

	public static synchronized void send(String name, File file, User user, TaskPopup taskPopup) throws IOException, InterruptedException, ExecutionException, ZipException
	{
		// compress
		if (taskPopup.isClosed())
		{
			return;
		}
		taskPopup.setText("Compressing...");
		File targetFile = Utility.generateTempFile();
		targetFile.delete();
		Utility.compressFile(file, targetFile);

		// index
		if (taskPopup.isClosed())
		{
			return;
		}
		taskPopup.setText("Indexing...");
		TransmissionPath path = new TransmissionPath(file.toPath());

		// send
		if (taskPopup.isClosed())
		{
			return;
		}
		taskPopup.setText("Sending " + file.getName() + " to " + user.getAsTag() + " ...");
		taskPopup.setProgress(0.0);
		MessageChannel channel = user.openPrivateChannel().complete();
		int written = 0;
		int chunkIndex = 0;
		List<TransmissionChunk> chunks = new ArrayList<>();
		byte[] buffer = new byte[chunkSize];
		try (InputStream input = new FileInputStream(targetFile))
		{
			while (true)
			{
				if (taskPopup.isClosed())
				{
					return;
				}
				int length = input.read(buffer);
				if (length <= 0)
				{
					break;
				}
				written += length;
				if (length < chunkSize)
				{
					buffer = Arrays.copyOf(buffer, length);
				}
				if (taskPopup.isClosed())
				{
					return;
				}
				String filename = name + ".part." + (chunkIndex + 1);
				Message message = channel.sendFile(buffer, filename).complete();
				chunks.add(new TransmissionChunk(chunkIndex++, message, filename, length));
				taskPopup.setProgress((1.0 / targetFile.length()) * written);
			}
		}
		// metadata
		if (taskPopup.isClosed())
		{
			return;
		}
		DiscordMessageObject<Transmission> discordTransmission = sendMetadata(channel, new Transmission(name, path, chunks));

		// clean
		targetFile.delete();

		// add download
		UltraTransmissionWizard.register(discordTransmission.getMessage());
	}

	public static void receive(Transmission transmission, Path destination, TaskPopup taskPopup) throws FileNotFoundException, IOException, InterruptedException, ExecutionException, ZipException
	{
		int totalSize = transmission.getChunks().stream().mapToInt(c -> c.getSize()).sum();
		int totalChunks = transmission.getChunks().size();

		File targetFile = Utility.generateTempFile();
		targetFile.delete();

		// downloa
		try (OutputStream output = new FileOutputStream(targetFile))
		{
			if (taskPopup.isClosed())
			{
				return;
			}
			int chunkIndex = 0;
			int totalWritten = 0;
			for (TransmissionChunk chunk : transmission.getChunks())
			{
				taskPopup.setText("Downloading chunk " + ++chunkIndex + " out of " + totalChunks);
				int length = -1;
				try (InputStream input = chunk.getAttachment().retrieveInputStream().get())
				{
					length = IOUtils.copy(input, output);
				}
				if (length >= 0)
				{
					totalWritten += length;
				}
				taskPopup.setProgress(1.0 / totalSize * totalWritten);
			}
		}

		// extract
		taskPopup.setText("Decompressing...");
		taskPopup.setProgress(-1);

		Utility.decompressFile(targetFile, destination);

		// clean
		targetFile.delete();
	}
}
