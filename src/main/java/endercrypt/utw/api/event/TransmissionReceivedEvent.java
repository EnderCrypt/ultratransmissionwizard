package endercrypt.utw.api.event;

import endercrypt.utw.api.DiscordMessageObject;
import endercrypt.utw.api.model.Transmission;

public interface TransmissionReceivedEvent
{
	public void onEvent(DiscordMessageObject<Transmission> transmissionMetadata);
}
