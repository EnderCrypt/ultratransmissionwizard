package endercrypt.utw.api.model;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.function.Predicate;

import endercrypt.utw.utility.Protector;
import endercrypt.utw.utility.Utility;
import javafx.scene.image.Image;

public enum PathType
{
	DIRECTORY(Files::isDirectory, "/gfx/Directory.png"),
	FILE(Files::isRegularFile, "/gfx/File.png");

	private final Predicate<Path> checker;
	private final Image image;

	private PathType(Predicate<Path> checker, String imagePath)
	{
		this.checker = checker;
		this.image = Protector.run(true, () -> {
			try (InputStream inputStream = Utility.getResourceStream(imagePath))
			{
				return new Image(inputStream);
			}
		}).getResult();
	}

	public Image getImage()
	{
		return image;
	}

	public static PathType determine(Path path)
	{
		for (PathType type : values())
		{
			if (type.checker.test(path))
			{
				return type;
			}
		}
		throw new IllegalArgumentException(path + " is not of " + Arrays.toString(PathType.values()));
	}
}
