package endercrypt.utw.api.model;

import com.google.gson.JsonObject;

import endercrypt.utw.discord.messagesearcher.DiscordMessageSearcher;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;

public class TransmissionChunk
{
	private int chunkIndex;
	private Message message;
	private String filename;
	private int size;

	public TransmissionChunk(int chunkIndex, Message message, String filename, int size)
	{
		this.chunkIndex = chunkIndex;
		this.message = message;
		this.filename = filename;
		this.size = size;
	}

	public TransmissionChunk(JsonObject json, DiscordMessageSearcher searcher)
	{
		chunkIndex = json.get("index").getAsInt();
		message = searcher.find(json.get("message").getAsLong());
		filename = json.get("filename").getAsString();
		size = json.get("size").getAsInt();
	}

	public int getChunkIndex()
	{
		return chunkIndex;
	}

	public Message getMessage()
	{
		return message;
	}

	public Attachment getAttachment()
	{
		return getMessage().getAttachments().get(0);
	}

	public String getFilename()
	{
		return filename;
	}

	public int getSize()
	{
		return size;
	}

	public JsonObject toJson()
	{
		JsonObject root = new JsonObject();

		root.addProperty("index", getChunkIndex());
		root.addProperty("message", getMessage().getIdLong());
		root.addProperty("filename", getFilename());
		root.addProperty("size", getSize());

		return root;
	}
}
