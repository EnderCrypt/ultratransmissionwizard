package endercrypt.utw.api.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import endercrypt.utw.discord.messagesearcher.DiscordDirectMessageSearcher;
import endercrypt.utw.discord.messagesearcher.DiscordMessageSearcher;
import endercrypt.utw.utility.JavaFxUtility;
import endercrypt.utw.utility.Utility;
import endercrypt.utw.utility.JavaFxUtility.TaskPopup;
import net.dv8tion.jda.api.entities.Message;

public class Transmission
{
	public static final String FILENAME = "UltraTransmissionWizard";

	private final String name;

	private long messageId;

	private final TransmissionPath path;

	private final List<TransmissionChunk> chunks;

	public Transmission(String name, TransmissionPath path, List<TransmissionChunk> chunks)
	{
		Objects.requireNonNull(name);
		Objects.requireNonNull(path);
		Objects.requireNonNull(chunks);

		this.name = name;
		this.messageId = -1;
		this.path = path;
		this.chunks = chunks;
	}

	public Transmission(JsonObject root, Message message)
	{
		Objects.requireNonNull(root);
		Objects.requireNonNull(message);
		name = root.get("name").getAsString();
		path = new TransmissionPath(root.get("path").getAsJsonObject());

		chunks = new ArrayList<>();

		TaskPopup taskPopup = JavaFxUtility.taskPopup("Assembling transmission \"" + name + "\"", "collecting chunks..", false);
		DiscordMessageSearcher messageSearcher = new DiscordDirectMessageSearcher(message.getChannel());

		JsonArray chunksArray = root.get("chunks").getAsJsonArray();
		int i = 0;
		for (JsonElement element : chunksArray)
		{
			chunks.add(new TransmissionChunk(element.getAsJsonObject(), messageSearcher));
			taskPopup.setProgress(1.0 / chunksArray.size() * ++i);
		}

		taskPopup.close();
	}

	public String getName()
	{
		return name;
	}

	public void setMessageId(long messageId)
	{
		if (this.messageId != -1)
		{
			throw new IllegalStateException();
		}
		this.messageId = messageId;
	}

	public long getMessageId()
	{
		return messageId;
	}

	public TransmissionPath getPath()
	{
		return path;
	}

	public List<TransmissionChunk> getChunks()
	{
		return Collections.unmodifiableList(chunks);
	}

	public JsonObject toJson()
	{
		JsonObject root = new JsonObject();
		root.addProperty("name", name);
		root.add("path", path.toJson());

		JsonArray chunksJson = new JsonArray();
		for (TransmissionChunk chunk : chunks)
		{
			chunksJson.add(chunk.toJson());
		}
		root.add("chunks", chunksJson);

		return root;
	}

	public String toEncodedJsonString()
	{
		String jsonText = Utility.gson.toJson(toJson());
		return jsonText;
	}
}
