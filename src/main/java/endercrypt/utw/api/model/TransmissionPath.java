package endercrypt.utw.api.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static endercrypt.utw.api.model.PathType.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import endercrypt.utw.utility.Utility;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

public class TransmissionPath
{
	private final String name;
	private final PathType type;
	private final long size;

	private List<TransmissionPath> children = new ArrayList<>();

	public TransmissionPath(Path path) throws IOException
	{
		this.name = path.getFileName().toString();
		this.type = PathType.determine(path);

		if (is(DIRECTORY))
		{
			for (Path subPath : Files.list(path).toArray(Path[]::new))
			{
				Path newPath = path.resolve(subPath);
				children.add(new TransmissionPath(newPath));
			}
		}

		this.size = is(PathType.FILE) ? Files.size(path) : children.stream().mapToLong(p -> p.getSize()).sum();
	}

	public TransmissionPath(JsonObject json)
	{
		this.name = json.get("name").getAsString();
		this.type = PathType.valueOf(json.get("type").getAsString());
		this.size = json.get("size").getAsLong();

		if (is(DIRECTORY))
		{
			for (JsonElement element : json.get("children").getAsJsonArray())
			{
				this.children.add(new TransmissionPath(element.getAsJsonObject()));
			}
		}
	}

	public boolean is(PathType type)
	{
		return this.type == type;
	}

	public String getName()
	{
		return name;
	}

	public PathType getType()
	{
		return type;
	}

	public long getSize()
	{
		return size;
	}

	public List<TransmissionPath> getChildren()
	{
		return Collections.unmodifiableList(children);
	}

	public TreeItem<TransmissionPath> toTreeItem()
	{
		TreeItem<TransmissionPath> me = new TreeItem<>(this);
		me.setGraphic(new ImageView(type.getImage()));
		if (is(FILE))
		{

		}
		if (is(DIRECTORY))
		{
			List<TreeItem<TransmissionPath>> treeItemChildren = children.stream().map(p -> p.toTreeItem()).collect(Collectors.toList());
			me.getChildren().addAll(treeItemChildren);
		}
		return me;
	}

	public JsonObject toJson()
	{
		JsonObject json = new JsonObject();

		json.addProperty("name", getName());
		json.addProperty("type", type.name());
		json.addProperty("size", getSize());

		if (is(DIRECTORY))
		{
			JsonArray jsonChildren = new JsonArray();

			for (TransmissionPath path : this.children)
			{
				jsonChildren.add(path.toJson());
			}

			json.add("children", jsonChildren);
		}

		return json;
	}

	public String toFilesizeString()
	{
		double mb = Math.pow(1024, 2);
		if (size >= (0.1 * mb))
		{
			return Utility.round(size / mb, 1) + " mb";
		}
		double kb = Math.pow(1024, 1);
		if (size > (10 * kb))
		{
			return Utility.round(size / kb, 1) + " kb";
		}
		return size + " bytes";
	}

	@Override
	public String toString()
	{
		return name + " (" + toFilesizeString() + ")";
	}
}
