package endercrypt.utw.api;

import net.dv8tion.jda.api.entities.Message;

public class DiscordMessageObject<T>
{
	private final T object;
	private final Message message;

	public DiscordMessageObject(T object, Message message)
	{
		this.object = object;
		this.message = message;
	}

	public T getObject()
	{
		return object;
	}

	public Message getMessage()
	{
		return message;
	}

	@Override
	public String toString()
	{
		return "[" + message.getIdLong() + " -> " + object + "]";
	}
}
